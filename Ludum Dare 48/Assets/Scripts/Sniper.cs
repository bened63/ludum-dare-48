using Bened;
using System.Collections;
using UnityEngine;

public class Sniper : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private GameObject _projectile;

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(IEShooting());
    }

    IEnumerator IEShooting()
    {
        while (true)
        {
            GameObject go = Instantiate(_projectile, transform.position, Quaternion.identity);
            Utils.LookAt2D(go.transform, _player.transform);
            Destroy(go, 5);
            yield return new WaitForSeconds(2);
        }
    }
}