using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private GameObject _light;
    [SerializeField] private ParticleSystem _moveFX;
    [SerializeField] private ParticleSystem _dashFX;

    [Header("Settings")]
    [SerializeField] private float _moveSpeed = 5;
    [SerializeField] private float _dashStrength = 100;
    [SerializeField] private float _dashVelocity = 1;
    [SerializeField] private int _hearts = 3;

    [SerializeField, Range(0.1f, 2f)] private float _attackSpeed = 1;
    [SerializeField] private float _pushBackStrength = 1000;
    [SerializeField] private float _dashDelay = 1;

    private Animator _animator;
    private Rigidbody2D _rb;
    private Vector3 _movement;
    private float _attackTimer = 0;
    private float _dashTimer = 0;
    private bool _canAttack;
    private bool _canMove = true;
    private bool _canDash = false;
    private bool _dash = false;
    private bool _inflated = false;
    private bool _godMode = false;
    private bool _lightPerk = false;

    public bool canMove { get => _canMove; set => _canMove = value; }
    public bool inflated { get => _inflated; set => _inflated = value; }
    public float pushBackStrength { get => _pushBackStrength; }
    public bool lightPerk { get => _lightPerk; set => _lightPerk = value; }

    // Start is called before the first frame update
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        // Attack timer
        // Check if Player can attack again
        _attackTimer += Time.deltaTime;
        if (_attackTimer > (1f / _attackSpeed)) _canAttack = true;

        // Move ParticleSystems
        _moveFX.transform.position = transform.position;
        _dashFX.transform.position = transform.position;

        // Dash delay
        _dashTimer += Time.deltaTime;
        if (_dashTimer > _dashDelay) _canDash = true;

        CalculateMovement();

        if (Input.GetKeyDown(KeyCode.Space) && _canAttack)
        {
            Attack();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && _canDash)
        {
            _dash = true;
        }

        // Set light on/off
        if (transform.position.y < -40 && transform.position.y > -45)
        {
            if (_lightPerk) _light.SetActive(true);
        }
        else if (transform.position.y > -40 && transform.position.y < -35)
        {
            _light.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (_canMove) Move();
    }

    private void Attack() => StartCoroutine(IEAttack());

    private IEnumerator IEAttack()
    {
        _canAttack = false;
        _attackTimer = 0;
        _inflated = true;
        _canMove = false;

        Debug.LogFormat("<color=lime>Attack</color>");
        _animator.SetTrigger("Attack");

        // When attack animation is finished Player can move again
        yield return new WaitForSeconds(1);
        _inflated = false;
        _canMove = true;
    }

    private void CalculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        _movement = new Vector3(horizontalInput, verticalInput, 0);
        _movement = Vector3.ClampMagnitude(_movement, 1);

        if (horizontalInput != 0 || verticalInput != 0)
        {
            _animator.SetBool("Move", true);
            var em = _moveFX.emission;
            em.enabled = true;
            //DoEmit(_moveFX); 
        }
        else
        {
            var em = _moveFX.emission;
            em.enabled = false;
            _animator.SetBool("Move", false);
        }
        // Animations
        //_animator.SetFloat("Horizontal", horizontalInput);
        //_animator.SetFloat("Vertical", verticalInput);
        //_animator.SetFloat("Speed", _movement.sqrMagnitude);
    }

    private void Move()
    {
        if (_dash)
        {
            _dashTimer = 0;
            _canDash = false;
            _dash = false;
            //_rb.AddForce(_movement * _dashStrength);
            _dashFX.Play();
            StartCoroutine(IEDash());
        }
        //transform.Translate(_movement * _moveSpeed * Time.deltaTime);
        _rb.velocity = _movement * _moveSpeed * Time.deltaTime * _dashVelocity;
    }

    private IEnumerator IEDash()
    {
        float currentLerpTime = 0f;
        float lerpTime = 0.1f;

        // increase velocity
        while (currentLerpTime < lerpTime)
        {
            //increment timer once per frame
            currentLerpTime =  Mathf.Clamp(currentLerpTime + Time.deltaTime, 0f, lerpTime);

            //lerp!
            float t = currentLerpTime / lerpTime;
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            _dashVelocity = Mathf.Lerp(1, _dashStrength, t); ;

            yield return null;
        }

        currentLerpTime = 0f;
        lerpTime = 1f;

        // decrease velocity
        while (currentLerpTime < lerpTime)
        {
            //increment timer once per frame
            currentLerpTime = Mathf.Clamp(currentLerpTime + Time.deltaTime, 0f, lerpTime);

            //lerp!
            float t = currentLerpTime / lerpTime;
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            _dashVelocity = Mathf.Lerp(_dashStrength, 1, t); ;

            yield return null;
        }
        yield return null;
    }

    public void CollectPearl()
    {
        Debug.LogFormat("<color=lime>Pear collected.</color>");
    }

    private void Hit(float damage)
    {
        Debug.LogFormat("<color=lime>Player hit by enemy. Get {0} damage.</color>", damage);
        IEGodModeForSeconds(1);
    }

    private IEnumerator IEGodModeForSeconds(float seconds)
    {
        _godMode = true;
        yield return new WaitForSeconds(seconds);
        _godMode = false;
    }

    void DoEmit(ParticleSystem system)
    {
        // Any parameters we assign in emitParams will override the current system's when we call Emit.
        // Here we will override the start color and size.
        var emitParams = new ParticleSystem.EmitParams();
        system.Emit(emitParams, 1);
        //system.Play(); // Continue normal emissions
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            if (!_inflated)
            {
                Enemy e = collision.gameObject.GetComponent<Enemy>();
                Vector2 direction = collision.transform.position - transform.position;
                _rb.AddForce(-direction.normalized * 800);
                Hit(e.damage);
            }
        }

        if (collision.transform.CompareTag("Obstacle"))
        {
            if (collision.relativeVelocity.magnitude > 5)
            {
                Obstacle o = collision.gameObject.GetComponent<Obstacle>();
                Hit(o.damage);
            }
        }
    }
}