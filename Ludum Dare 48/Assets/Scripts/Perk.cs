using UnityEngine;

public class Perk : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            p.lightPerk = true;
            Destroy(gameObject);
        }
    }
}