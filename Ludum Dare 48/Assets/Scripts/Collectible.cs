using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            p.CollectPearl();
            Destroy(gameObject);
        }
    }
}
