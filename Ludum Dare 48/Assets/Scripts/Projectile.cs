using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float _speed = 2;

    // Update is called once per frame
    private void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * _speed);
    }
}