using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Vision _vision;
    [SerializeField] private GameObject _dieFX;
    [SerializeField] private float _hitPoints = 2;
    [SerializeField] private float _damage = 1;
    private Rigidbody2D _rb;

    public float damage { get => _damage; }

    // Start is called before the first frame update
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.AddForce(Vector2.up);
        _vision.onPlayerInSight += HandleOnPlayerInSight;
    }

    private void HandleOnPlayerInSight(Vector3 v)
    {
        Vector2 direction = transform.position - v;
        _rb.AddForce(-direction.normalized * 6);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void Hit(float damage = 1)
    {
        Debug.LogFormat("<color=lime>Enemy gets {0} damage.</color>", damage);
        _hitPoints = Mathf.Clamp(_hitPoints - damage, 0, 9999);
        if (_hitPoints <= 0) Die();
    }

    void Die()
    {
        GameObject go = Instantiate(_dieFX, transform.position, Quaternion.identity);
        Destroy(go, 2);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            if (p.inflated)
            {
                Vector2 direction = collision.transform.position - transform.position;
                _rb.AddForce(-direction.normalized * p.pushBackStrength);
                Hit(1);
            }

            if (collision.transform.CompareTag("Obstacle"))
            {
                if (collision.relativeVelocity.magnitude > 5)
                {
                    Obstacle o = collision.gameObject.GetComponent<Obstacle>();
                    Hit(o.damage);
                }
            }
        }
    }
}