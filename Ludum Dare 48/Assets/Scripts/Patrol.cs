using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    [SerializeField] private float _speed = 1;
    [SerializeField] private float _minX, _maxX;

    private Vector3 _direction;

    // Start is called before the first frame update
    void Start()
    {
        _direction = transform.right;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Application.isPlaying)
            return;

        if (transform.position.x <= _minX || transform.position.x >= _maxX) Flip();
        transform.Translate(_direction * Time.deltaTime * _speed);
    }

    //private void FixedUpdate()
    //{
    //    Vector2 direction = collision.transform.position - transform.position;
    //    _rb.AddForce(-direction.normalized * p.pushBackStrength);
    //}

    private void Flip()
    {
        //_direction = -_direction;
        transform.Rotate(0, 180, 0);
    }
}
