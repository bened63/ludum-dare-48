using System;
using UnityEngine;

public class Vision : MonoBehaviour
{
    private Vector3 _playerPosition;

    public Vector3 playerPosition { get => _playerPosition; }

    public Action<Vector3> onPlayerInSight;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.LogFormat("<color=lime>Player is in sight.</color>");
            _playerPosition = collision.GetComponent<Player>().transform.position;
            onPlayerInSight?.Invoke(_playerPosition);
        }
    }
}