# Ludum Dare 48

Unfinished prototype project of an underwater siderscroller for Ludum Dare 48: https://ldjam.com/events/ludum-dare/48/

Theme: Deeper and deeper  
Saturday April 24th to Tuesday April 27th, 2021  
Starts at 03:00 MESZ *

## Screenshots

![](images/screenshot-01.jpg)
![](images/screenshot-02.jpg)
